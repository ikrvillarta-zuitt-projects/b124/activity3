package com.villarta;

public class User {
    private String firstName;
    private String lastName;
    private String contactNumber;
    String newline = System.getProperty("line.separator");

    public User(){
        System.out.println("Empty Constructor");
    }

    public User(String newFirstName, String newLastName, String newContactNumber) {
        this.firstName = newFirstName;
        this.lastName = newLastName;
        this.contactNumber = newContactNumber;
    }

    public String getFirstName(){

        return this.firstName;
    }
    public String getLastName(){

        return this.lastName;
    }
    public String getContactNumber(){

        return this.contactNumber;
    }

    public void setFirstName(String newFirstName){

        this.firstName = newFirstName;
    }
    public void setLastName(String newLastName){
        this.firstName = newLastName;
    }
    public void setContactNumber(String newContactNumber){
        this.contactNumber = newContactNumber;
    }

    public String userDescription(){
        return "Name: " + this.getFirstName() + newline +"Lastname: " + this.getLastName() + newline + "Contact " +
                "Number: " + this.getContactNumber();
    }

}
