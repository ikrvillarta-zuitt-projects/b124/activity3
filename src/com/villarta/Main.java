package com.villarta;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        User person1 = new User("Ian", "Villarta", "09663392059");
        System.out.println(person1.userDescription());

        System.out.println(" ******** ");

        User person2 = new User("Lors", "Dignadice", "09957442044");
        System.out.println(person2.userDescription());

        System.out.println(" ******** ");

        ArrayList<String> User = new ArrayList<String>();
        User.add(person1.getFirstName());
        User.add((person1.getLastName()));
        User.add(person1.getContactNumber());
        User.add(person2.getFirstName());
        User.add(person2.getLastName());
        User.add(person2.getContactNumber());

        for (String user : User){
            System.out.println(user);
        }







    }
}
